#!/usr/bin/env python

import argparse, ROOT
from .lhcbstyle import LHCbStyle

def open_and_plot():
    parser = argparse.ArgumentParser(description = "Print all event types in a dataset metadata file")
    parser.add_argument("filename", help = "ROOT file containing the object you want to draw")
    parser.add_argument("object", help = "Path to the object within the ROOT file")
    parser.add_argument("output", default = "plot", help="Output filename without file extension")
    parser.add_argument("-D", "--draw-style", default = "", help="Only print MC event types")
    parser.add_argument("-F", "--formats", nargs = "+", default = ["pdf", "png", "eps"], help = "Output file formats")
    parser.add_argument("--lhcb", action = "store_true", help = "Draw \"LHCb\" label")
    args = parser.parse_args()
    ROOT.gROOT.SetBatch(True)
    # Plot with LHCbStyle
    with LHCbStyle() as lbs:
        input_file = ROOT.TFile.Open(args.filename)
        plotted_obj = input_file.Get(args.object)
        plotted_obj.Draw(args.draw_style)
        if args.lhcb:
            ROOT.LHCbStyle.lhcbName.Draw("same")
        for ext in args.formats:
            ROOT.gPad.SaveAs(f"{args.output}.{ext}")
