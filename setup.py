import setuptools, subprocess, os

with open("README.md", "r", encoding = "utf-8") as readme:
	long_description = readme.read()

macro_dir = os.environ.get("ROOT_MACRO_DIR")
if not macro_dir:
	macro_dir = subprocess.check_output(["root", "-x", "-n", "-b", "-l", "-q", "-e", "{std::cout << gROOT->GetMacroDir() << std::endl;}"]).strip()
	if not os.path.isdir(macro_dir):
		raise ValueError("Failed to guess the ROOT macro directory, please set the ROOT_MACRO_DIR environment variable")
	if not os.access(macro_dir, os.W_OK):
		writable_macro_dir = os.path.join("share", "root", "macros")
		print(f"Unable to place macros in the ROOT macro directory: {macro_dir}")
		print(f"They will be placed here instead: {writable_macro_dir}")
		print("Please add this to Unix.*.Root.MacroPath in your .rootrc file.")
		macro_dir = writable_macro_dir

setuptools.setup(
	name = "lhcbstyle",
	version = "0.1",
	author = "Adam Morris",
	author_email = "adam.morris@cern.ch",
	description = "LHCb style for ROOT plots",
	long_description = long_description,
	long_description_content_type = "text/markdown",
	url = "https://gitlab.cern.ch/lhcb-docs/lhcbstyle",
	packages = setuptools.find_packages(),
	classifiers = [
		"Programming Language :: Python :: 3",
		"OSI Approved :: GNU General Public License v3 (GPLv3)",
		"Operating System :: OS Independent",
	],
	entry_points = {
		"console_scripts": [
			"lbplot=lhcbstyle.utils:open_and_plot"
		],
	},
	data_files = [
		(macro_dir, ["macros/lhcbStyle.C"])
	]
)

