#!/usr/bin/env python

import ROOT
from lhcbstyle import LHCbStyle

xlow, xup = (5.100, 5.460)

def generate_data(nsig = 1000, nbkg = 1000):
	for i in range(nsig):
		yield ROOT.gRandom.Gaus(5.280, 0.02)
	for i in range(nbkg):
		yield ROOT.gRandom.Uniform(xlow, xup)

data = list(generate_data())

def make_plot():
	hist = ROOT.TH1D("hist", "#it{B}^{0} mass", 36, xlow, xup)
	for d in data:
		hist.Fill(d)
	binw = hist.GetXaxis().GetBinWidth(1)
	hist.SetYTitle(f"Candidates / ({binw:.3f} GeV)")
	hist.SetXTitle("#it{m}(#it{B}^{0}) [GeV]")
	return hist

# Plot without LHCbStyle
can = ROOT.TCanvas()
hist = make_plot()
hist.Draw("E1")
can.SaveAs("hist_plain.png")

hist.SaveAs("hist_plain.root")

# Plot with LHCbStyle
with LHCbStyle() as lbs:
	can = ROOT.TCanvas()
	hist = make_plot()
	hist.Draw("E1")
	ROOT.LHCbStyle.lhcbName.Draw("same")
	can.SaveAs("hist_lhcb.png")

